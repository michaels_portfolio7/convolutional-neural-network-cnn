# PyTorch Example: Convolutional Neural Network

This repository contains an example of training a convolutional neural network (CNN) using PyTorch. We use the CIFAR-10 dataset to train a CNN with two convolutional layers, two pooling layers, and three fully connected layers.

## Requirements

To run this example, you need to have PyTorch and torchvision installed. You can install them using pip:
    
    pip install torch torchvision 

## Usage

To train the CNN, simply run the `train.py` script:
    
    python train.py 

The script will download the CIFAR-10 dataset (if it's not already downloaded) and train the CNN for two epochs. The output will show the running loss every 2000 mini-batches.

## Customization

You can customize the CNN architecture by editing the `Net` class in the `net.py` file. You can also change the hyperparameters of the optimizer and the number of epochs in the `train.py` script.

## Dataset

The CIFAR-10 dataset consists of 60000 32x32 color images in 10 classes, with 6000 images per class. There are 50000 training images and 10000 test images.

## License

This repository is licensed under the MIT License. See the `LICENSE` file for more information.

## References

* PyTorch documentation: [https://pytorch.org/docs/stable/index.html](https://pytorch.org/docs/stable/index.html)
* CIFAR-10 dataset: [https://www.cs.toronto.edu/~kriz/cifar.html](https://www.cs.toronto.edu/~kriz/cifar.html)